from django.contrib import admin

from .models import Configuration, Course, Section, Student, Attendance, Assignment, GradeBook, Question, Answer

admin.site.register(Configuration)
admin.site.register(Course)
admin.site.register(Section)
admin.site.register(Student)
admin.site.register(Attendance)
admin.site.register(Assignment)
admin.site.register(GradeBook)
admin.site.register(Question)
admin.site.register(Answer)
