from django.apps import AppConfig


class MathlabConfig(AppConfig):
    name = 'mathlab'
