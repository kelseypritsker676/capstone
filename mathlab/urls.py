from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^logout$', views.logout_view, name='logout'),
    url(r'^ForgottenPassword/$', views.ForgottenPassword, name='forgottenpassword'),
    url(r'^NewPassword/$', views.NewPassword, name='newpassword'),
    url(r'^CreateUser/$', views.CreateUser, name='createuser'),
    # Student
    url(r'^Student/StudentHome/$', views.StudentHome, name='studenthome'),
    url(r'^Student/StudentAttendanceOverview/$', views.StudentAttendanceOverview, name='studentattendanceoverview'),
    url(r'^Student/StudentQuizOverview/$', views.StudentQuizOverview, name='studentquizoverview'),
    url(r'^Student/QuizQuestion/$', views.QuizQuestion, name='quizquestion'),
    # Professor
    url(r'^Professor/ProfessorHome/$', views.ProfessorHome, name='professorhome'),
    url(r'^Professor/Roster/$', views.Roster, name='roster'),
    # LabAssistant
    url(r'^LabAssistant/LabAssistantHome/$', views.LabAssistantHome, name='labassistanthome'),
    url(r'^LabAssistant/LabAssistantStudentSearch/$', views.LabAssistantStudentSearch, name='labassistantstudentsearch'),
    url(r'^LabAssistant/LabAssistantStudentSearch/StudentSearch/$', views.StudentSearch, name='studentsearch'),
    url(r'^LabAssistant/LabAssistantStudentSearch/AddStudentAttendance/$]',
        views.AddStudentAttendance, name='addstudentattendance'),
    # TestingViews
    url(r'^DispMeta/$', views.display_meta, name='dispmeta'),
]