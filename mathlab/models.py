from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import User, Group


@python_2_unicode_compatible
class Configuration(models.Model):
    weeks_total = models.IntegerField()


@python_2_unicode_compatible
class Course(models.Model):
    course_code = models.CharField(max_length=10)
    course_name = models.CharField(max_length=60)

    def __str__(self):
        return 'Course: {}'.format(self.course_code)


@python_2_unicode_compatible
class Section(models.Model):
    section_num = models.IntegerField()
    course = models.ForeignKey(Course)
    professor = models.ForeignKey(User)
    meeting_info = models.CharField(max_length=20)

    def __str__(self):
        return 'Course: {} Section: {}'.format(self.course.course_code, self.section_num)


@python_2_unicode_compatible
class Student(models.Model):
    student = models.ForeignKey(User, on_delete=models.CASCADE)
    section = models.ForeignKey(Section)

    def __str__(self):
        return 'Student: {} {}'.format(self.student.first_name, self.student.last_name)


@python_2_unicode_compatible
class Attendance(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    att_week_num = models.IntegerField()


@python_2_unicode_compatible
class Assignment(models.Model):
    assignment_week = models.IntegerField()
    # 1, 2, or 3
    assignment_section = models.IntegerField()
    course = models.ForeignKey(Course)

    def __str__(self):
        return 'Assignment: Week {} Section {}'.format(self.assignment_week, self.assignment_section)


@python_2_unicode_compatible
class Question(models.Model):
    question_text = models.CharField(max_length=1000)
    assignment = models.ForeignKey(Assignment)

    def __str__(self):
        return 'Question {}: Week {} Section {}'.format(self.id, self.assignment.assignment_week,
                                                     self.assignment.assignment_section)


@python_2_unicode_compatible
class GradeBook(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)


@python_2_unicode_compatible
class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer_text = models.CharField(max_length=500, default="Answer text here.")
    # 0 = incorrect, 1 = correct
    answer_correctness = models.IntegerField()

    def __str__(self):
        return 'Answer {}: Week {} Section {} Question {}'.format(self.id, self.question.assignment.assignment_week,
                                                   self.question.assignment.assignment_section, self.question.id)
