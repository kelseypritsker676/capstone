from django import forms
from django.forms import PasswordInput, ModelForm
from .models import Course, Section, Student, Attendance, Assignment, GradeBook, Question, Answer


class LoginForm(forms.Form):
    username = forms.CharField(label='', max_length=30, widget=forms.TextInput(attrs={'class': 'form-control',
                                                                                      'placeholder': 'Username'}))
    password = forms.CharField(label='', widget=PasswordInput(attrs={'class': 'form-control'}))


class ChangePasswordForm(forms.Form):
    password = forms.CharField(label='', widget=PasswordInput(attrs={'class': 'form-control'}))
    password_repeat = forms.CharField(label='', widget=PasswordInput(attrs={'class': 'form-control'}))
