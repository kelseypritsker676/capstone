

from django.shortcuts import get_object_or_404, render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib.auth.decorators import login_required
from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import Http404
from django.contrib.auth.decorators import user_passes_test
from django.db.models import Sum, Count, Max
import random
import json

from .forms import *
from .models import *


def index(request):
    success_message = ''
    error_message = ''

    if request.method == 'POST':
        if 'signin' in request.POST:
            form = LoginForm(request.POST)
            if form.is_valid():
                user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        if user.groups.filter(name='Student').exists():
                            return redirect('/mathlab/Student/StudentHome')
                        elif user.groups.filter(name='Lab Assistant').exists():
                            return redirect('/mathlab/LabAssistant/LabAssistantHome')
                        elif user.groups.filter(name='Professor').exists():
                            return redirect('/mathlab/Professor/ProfessorHome')
                        else:
                            error_message += 'You are not in any group. Please contact an administrator.'
                    else:
                        error_message += 'Your account is not currently active. Please contact an administrator.'
                else:
                    error_message += 'Failed to authenticate. Please check your username/password.'
            else:
                error_message += form.errors
    login_form = LoginForm()
    return render(
        request,
        'mathlab/HomePage.html',
        {'login_form': login_form,
         'success_message': success_message,
         'error_message': error_message
         }
    )


def logout_view(request):
    logout(request)
    return redirect('/mathlab/')


def ForgottenPassword(request):
    return render(request, 'mathlab/ForgottenPassword.html')


def NewPassword(request):
    return render(request, 'mathlab/NewPassword.html')


def CreateUser(request):
    return render(request, 'mathlab/CreateUser.html')


# Student
def student_check(user):
    return user.is_superuser or user.groups.filter(name='Student').exists()


@user_passes_test(student_check)
def StudentHome(request):
    template = loader.get_template('mathlab/Student/StudentHome.html')
    student = Student.objects.get(student_id=request.user.id)
    context = {
        'current_location': '/StudentHome/',
        'section': student.section,
        'course': student.section.course,
        'professor': student.section.professor,
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(student_check)
def StudentAttendanceOverview(request):
    template = loader.get_template('mathlab/Student/StudentAttendanceOverview.html')
    student = Student.objects.get(student_id=request.user.id)
    week = Configuration.objects.get(id=1)
    student_attendance = {i: 0 for i in range(1, week.weeks_total + 1)}
    attendance = Attendance.objects.values('att_week_num').filter(student_id=student.id)
    for att in attendance:
        if att['att_week_num'] in student_attendance:
            student_attendance[att['att_week_num']] = 1
    context = {
        'student_attendance': student_attendance,
        'current_location': '/StudentAttendanceOverview/',
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(student_check)
def StudentQuizOverview(request):
    template = loader.get_template('mathlab/Student/StudentQuizOverview.html')
    student = Student.objects.get(student_id=request.user.id)
    total_weeks = Configuration.objects.get(id=1).weeks_total
    assignment_data = {}
    for week in range(1, total_weeks + 1):
        assignment_data[week] = []
        assignments = Assignment.objects.filter(assignment_week=week).order_by('assignment_section')
        for assignment in assignments:
            complete = GradeBook.objects.filter(student_id=student.id,
                                                question__assignment_id=assignment.id).count() >= 3

            assignment_data[week].append((assignment.id, assignment.assignment_section, complete))

    context = {
        'assignment_data': assignment_data,
        'current_location': '/StudentQuizOverview/',
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(student_check)
def QuizQuestion(request):
    if request.method == "GET":
        student = Student.objects.get(student_id=request.user.id)
        assignment = Assignment.objects.get(id=request.GET['assign_id'])
        answered = GradeBook.objects.filter(student_id=student.id, question__assignment=assignment.id)\
            .values('question_id')

        questions = Question.objects.filter(assignment_id=assignment.id).exclude(id__in=answered)

        if len(answered) == 3 or len(questions) == 0:
            if 'update' in request.GET:
                return HttpResponse(json.dumps({'answered': len(answered)}), content_type='application/json')
            else:
                return redirect('/mathlab/Student/StudentQuizOverview')

        # Assume that there is at least 1 question that they still need to answer, otherwise they should not
        # be on the page.
        next_question = random.choice(questions)
        answers = Answer.objects.filter(question_id=next_question.id).all()
        if 'update' in request.GET:
            question_data = {'question_text': next_question.question_text, 'answers': [], 'answered': len(answered)}
            for answer in answers:
                question_data['answers'].append((answer.id, answer.answer_text))
            return HttpResponse(json.dumps(question_data), content_type='application/json')
        else:
            context = {
                'assignment': assignment,
                'question_text': next_question.question_text,
                'answers': answers,
                'answered': len(answered),
            }
            return render(request, 'mathlab/Student/QuizQuestion.html', context)
    elif request.method == "POST":
        response_data = {}
        student = Student.objects.get(student_id=request.user.id)
        post_answer = int(request.POST['radioAnswer'])
        answer = Answer.objects.get(id=post_answer)
        if answer.answer_correctness:
            response_data['success'] = True
            grade = GradeBook(student_id=student.id, question_id=answer.question_id)
            grade.save()
        else:
            response_data['success'] = False
        return HttpResponse(json.dumps(response_data), content_type='application/json')


# Professor
def professor_check(user):
    return user.is_superuser or user.groups.filter(name='Professor').exists()


@user_passes_test(professor_check)
def ProfessorHome(request):
    template = loader.get_template('mathlab/Professor/ProfessorHome.html')
    section = Section.objects.get(professor_id=request.user.id)
    context = {
        'current_location': '/ProfessorHome/',
        'section': section,
        'course': section.course,
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(professor_check)
def Roster(request):
    template = loader.get_template('mathlab/Professor/Roster.html')
    sections = Section.objects.filter(professor=request.user.id).all()

    # Fill in student information for each of the professors sections
    teacher_rosters = []
    for section in sections:
        students = Student.objects.filter(section_id=section.id).all()
        student_data = []

        # Fill in student grade and attendance information
        for student in students:
            attendance = Attendance.objects.filter(student_id=student.id).count()
            grades = GradeBook.objects.filter(student_id=student.id).count()

            student_data.append((student.student.first_name,student.student.last_name, attendance, grades))

        total_assignments = int(Assignment.objects.filter(course_id=section.course.id).all().count() * 3)
        teacher_rosters.append((section.course.course_code, section.section_num,  student_data, total_assignments))

    context = {
        'current_location': '/Roster/',
        'roster_list': teacher_rosters,
    }

    return HttpResponse(template.render(context, request))


# Lab Assistant
def lab_assistant_check(user):
    return user.is_superuser or user.groups.filter(name='Lab Assistant').exists()


@user_passes_test(lab_assistant_check)
def LabAssistantHome(request):
    template = loader.get_template('mathlab/LabAssistant/LabAssistantHome.html')
    context = {
        'current_location': '/LabAssistantHome/',
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(lab_assistant_check)
def LabAssistantStudentSearch(request):
    template = loader.get_template('mathlab/LabAssistant/LabAssistantStudentSearch.html')
    context = {
        'current_location': '/LabAssistantStudentSearch/',
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(lab_assistant_check)
def StudentSearch(request):
    if request.is_ajax():
        first_name = request.GET.get('fn', '')
        last_name = request.GET.get('ln', '')
        if not first_name and not last_name:
            return HttpResponse(json.dumps({}), content_type='application/json')
        weeks = Configuration.objects.get(id=1)
        student_data = {'weeks': weeks.weeks_total, 'data': []}
        students = Student.objects.filter(student__first_name__icontains=first_name
                                          , student__last_name__icontains=last_name)
        for student in students:
            user = User.objects.get(id=student.student_id)
            student_data['data'].append({'id': student.id, 'first_name': user.first_name,
                                         'last_name': user.last_name, 'email': user.email,
                                         'username': user.username})
        return HttpResponse(json.dumps(student_data), content_type='application/json')
    else:
        return HttpResponse(json.dumps({}), content_type='application/json')


@user_passes_test (lab_assistant_check)
def AddStudentAttendance (request):
    if request.is_ajax():
        student = Student.objects.get(student_id=request.user.id)
        first_name = request.GET.get('fn', '')
        last_name = request.GET.get('ln', '')
        if not first_name and not last_name:
            return HttpResponse(json.dumps({}), content_type='application/json')
        student_data = []
        students = Student.objects.filter(student__first_name__icontains=first_name,
                                          student__last_name__icontains=last_name)
        for student in students:
            user = User.objects.get(id=student.student_id)
            student_data.append({'id': student.id, 'first_name': user.first_name,
                                 'last_name': user.last_name, 'email': user.email,
                                 'username': user.username})
        return HttpResponse(json.dumps(student_data), content_type='application/json')
    else:
        return HttpResponse(json.dumps({}), content_type='application/json')


# Texting Views
def display_meta(request):
    values = request.META.items()
    values.sort()
    html = []
    for k, v in values:
        html.append('<tr><td>%s</td><td>%s</td></tr>' % (k, v))
    return HttpResponse('<table>%s</table>' % '\n'.join(html))
