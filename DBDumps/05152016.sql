CREATE DATABASE  IF NOT EXISTS `mathlabdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mathlabdb`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: localhost    Database: mathlabdb
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (3,'Lab Assistant'),(2,'Professor'),(1,'Student');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
INSERT INTO `auth_group_permissions` VALUES (1,1,16),(2,1,17),(3,1,42),(4,2,42),(5,3,10),(6,3,11),(7,3,12);
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add course',1,'add_course'),(2,'Can change course',1,'change_course'),(3,'Can delete course',1,'delete_course'),(4,'Can add section',2,'add_section'),(5,'Can change section',2,'change_section'),(6,'Can delete section',2,'delete_section'),(7,'Can add student',3,'add_student'),(8,'Can change student',3,'change_student'),(9,'Can delete student',3,'delete_student'),(10,'Can add attendance',4,'add_attendance'),(11,'Can change attendance',4,'change_attendance'),(12,'Can delete attendance',4,'delete_attendance'),(13,'Can add assignment',5,'add_assignment'),(14,'Can change assignment',5,'change_assignment'),(15,'Can delete assignment',5,'delete_assignment'),(16,'Can add grade book',6,'add_gradebook'),(17,'Can change grade book',6,'change_gradebook'),(18,'Can delete grade book',6,'delete_gradebook'),(19,'Can add question',7,'add_question'),(20,'Can change question',7,'change_question'),(21,'Can delete question',7,'delete_question'),(22,'Can add answer',8,'add_answer'),(23,'Can change answer',8,'change_answer'),(24,'Can delete answer',8,'delete_answer'),(25,'Can add log entry',9,'add_logentry'),(26,'Can change log entry',9,'change_logentry'),(27,'Can delete log entry',9,'delete_logentry'),(28,'Can add permission',10,'add_permission'),(29,'Can change permission',10,'change_permission'),(30,'Can delete permission',10,'delete_permission'),(31,'Can add group',11,'add_group'),(32,'Can change group',11,'change_group'),(33,'Can delete group',11,'delete_group'),(34,'Can add user',12,'add_user'),(35,'Can change user',12,'change_user'),(36,'Can delete user',12,'delete_user'),(37,'Can add content type',13,'add_contenttype'),(38,'Can change content type',13,'change_contenttype'),(39,'Can delete content type',13,'delete_contenttype'),(40,'Can add session',14,'add_session'),(41,'Can change session',14,'change_session'),(42,'Can delete session',14,'delete_session');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$24000$jM7s8tEPqvv0$FsqOHJiwX4Dngro5+4FtBlOhAlZdu7Z63Mq9n3qSNig=','2016-05-05 07:17:14.078871',1,'kelsey','','','kelsey.pritsker676@myci.csuci.edu',1,1,'2016-05-02 05:22:17.803269'),(2,'pbkdf2_sha256$24000$bt6cBRSK0mHa$R7GmFLvdwZJPEC7Vo+93nc13MbchvfvJzLLwjM5OjJk=','2016-05-04 02:13:10.708330',0,'john.smith000','John','Smith','john.smith000@myci.csuci.edu',0,1,'2016-05-02 08:23:59.000000'),(3,'pbkdf2_sha256$24000$OCT0mJpPdIcL$5wvWUlSdcwIsVt9dmX5uHWchhKQ+FGY2zhDXSTlx0Q0=','2016-05-05 05:52:17.359672',0,'Jane.Doe','Jane','Doe','jane.doe@csuci.edu',0,1,'2016-05-02 22:51:30.000000'),(4,'pbkdf2_sha256$24000$DeG6bzb49PHK$tFJIBuI08jZKAMUuDEOel89Z7yCmjwpbtGMy/YnQZGs=','2016-05-03 06:49:14.182808',0,'Frank.Underwood','Frank','Underwood','frank.underwood@gmail.com',0,1,'2016-05-02 23:37:16.000000');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
INSERT INTO `auth_user_groups` VALUES (1,2,1),(2,3,2),(3,4,3);
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
INSERT INTO `auth_user_user_permissions` VALUES (1,4,10),(2,4,11),(3,4,12);
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2016-05-02 08:20:46.424135','1','Student',1,'Added.',11,1),(2,'2016-05-02 08:23:59.791372','2','john.smith000',1,'Added.',12,1),(3,'2016-05-02 08:26:00.795920','2','john.smith000',2,'Changed first_name, last_name, email, groups and last_login.',12,1),(4,'2016-05-02 08:26:17.526676','2','john.smith000',2,'No fields changed.',12,1),(5,'2016-05-02 08:27:41.973940','1','Course object',1,'Added.',1,1),(6,'2016-05-02 08:28:26.340660','2','Course object',1,'Added.',1,1),(7,'2016-05-02 22:49:06.836485','2','Professor',1,'Added.',11,1),(8,'2016-05-02 22:50:32.363670','3','Lab Assistant',1,'Added.',11,1),(9,'2016-05-02 22:51:30.855060','3','Jane.Doe',1,'Added.',12,1),(10,'2016-05-02 22:52:28.995259','3','Jane.Doe',2,'Changed first_name, last_name, email and groups.',12,1),(11,'2016-05-02 22:53:31.558041','1','Course: MATH 94 Section: 1',1,'Added.',2,1),(12,'2016-05-02 22:53:35.633880','1','Course: MATH 94 Section: 1',2,'No fields changed.',2,1),(13,'2016-05-02 22:54:03.719729','1','Student: John Smith',1,'Added.',3,1),(14,'2016-05-02 23:01:03.200210','2','john.smith000',2,'Changed password.',12,1),(15,'2016-05-02 23:37:16.670478','4','Frank.Underwood',1,'Added.',12,1),(16,'2016-05-02 23:38:17.120866','4','Frank.Underwood',2,'Changed groups and user_permissions.',12,1),(17,'2016-05-02 23:38:36.057584','4','Frank.Underwood',2,'Changed first_name, last_name and email.',12,1),(18,'2016-05-05 07:19:08.811778','1','Assignment: Week 1 Section 1',1,'Added.',5,1),(19,'2016-05-05 07:19:16.099388','2','Assignment: Week 1 Section 2',1,'Added.',5,1),(20,'2016-05-05 07:19:30.682447','3','Assignment: Week 1 Section 3',1,'Added.',5,1),(21,'2016-05-05 07:21:32.955043','1','Question: Week 1 Section 1',1,'Added.',7,1),(22,'2016-05-05 07:23:38.513225','1','Answer: Week 1 Section 1',1,'Added.',8,1),(23,'2016-05-05 07:23:50.613700','2','Answer: Week 1 Section 1',1,'Added.',8,1),(24,'2016-05-05 07:24:03.190444','3','Answer: Week 1 Section 1',1,'Added.',8,1),(25,'2016-05-05 07:24:29.507289','4','Answer: Week 1 Section 1',1,'Added.',8,1),(26,'2016-05-05 07:27:06.530429','2','Question 2: Week 1 Section 1',1,'Added.',7,1),(27,'2016-05-05 07:27:55.151374','5','Answer: Week 1 Section 1',1,'Added.',8,1),(28,'2016-05-05 07:28:05.185772','6','Answer: Week 1 Section 1',1,'Added.',8,1),(29,'2016-05-05 07:28:17.244983','7','Answer: Week 1 Section 1',1,'Added.',8,1),(30,'2016-05-05 07:28:29.353560','8','Answer: Week 1 Section 1',1,'Added.',8,1),(31,'2016-05-05 07:29:12.222507','3','Question 3: Week 1 Section 1',1,'Added.',7,1),(32,'2016-05-05 07:30:32.830024','9','Answer: Week 1 Section 1',1,'Added.',8,1),(33,'2016-05-05 07:30:52.823832','10','Answer: Week 1 Section 1',1,'Added.',8,1),(34,'2016-05-05 07:31:06.935507','11','Answer: Week 1 Section 1',1,'Added.',8,1),(35,'2016-05-05 07:31:25.606976','12','Answer: Week 1 Section 1',1,'Added.',8,1),(36,'2016-05-05 07:37:58.113190','4','Question 4: Week 1 Section 2',1,'Added.',7,1),(37,'2016-05-05 07:39:54.425491','13','Answer 13: Week 1 Section 2 Question 4',1,'Added.',8,1),(38,'2016-05-05 07:40:05.791603','14','Answer 14: Week 1 Section 2 Question 4',1,'Added.',8,1),(39,'2016-05-05 07:40:14.692003','15','Answer 15: Week 1 Section 2 Question 4',1,'Added.',8,1),(40,'2016-05-05 07:40:25.226449','16','Answer 16: Week 1 Section 2 Question 4',1,'Added.',8,1),(41,'2016-05-05 07:41:24.229332','5','Question 5: Week 1 Section 2',1,'Added.',7,1),(42,'2016-05-05 07:45:52.993606','17','Answer 17: Week 1 Section 2 Question 5',1,'Added.',8,1),(43,'2016-05-05 07:46:02.254184','18','Answer 18: Week 1 Section 2 Question 5',1,'Added.',8,1),(44,'2016-05-05 07:46:12.771998','19','Answer 19: Week 1 Section 2 Question 5',1,'Added.',8,1),(45,'2016-05-05 07:46:23.622080','20','Answer 20: Week 1 Section 2 Question 5',1,'Added.',8,1),(46,'2016-05-05 07:48:38.598731','6','Question 6: Week 1 Section 2',1,'Added.',7,1),(47,'2016-05-05 07:50:25.955976','21','Answer 21: Week 1 Section 2 Question 6',1,'Added.',8,1),(48,'2016-05-05 07:50:43.812118','22','Answer 22: Week 1 Section 2 Question 6',1,'Added.',8,1),(49,'2016-05-05 07:50:59.328374','23','Answer 23: Week 1 Section 2 Question 6',1,'Added.',8,1),(50,'2016-05-05 07:51:04.280804','23','Answer 23: Week 1 Section 2 Question 6',2,'Changed answer_text.',8,1),(51,'2016-05-05 07:51:12.682212','22','Answer 22: Week 1 Section 2 Question 6',2,'Changed answer_text.',8,1),(52,'2016-05-05 07:51:31.472242','21','Answer 21: Week 1 Section 2 Question 6',2,'Changed answer_text.',8,1),(53,'2016-05-05 07:51:40.193214','22','Answer 22: Week 1 Section 2 Question 6',2,'Changed answer_text.',8,1),(54,'2016-05-05 07:51:46.420832','23','Answer 23: Week 1 Section 2 Question 6',2,'Changed answer_text.',8,1),(55,'2016-05-05 07:54:55.748291','7','Question 7: Week 1 Section 3',1,'Added.',7,1),(56,'2016-05-05 07:57:11.271641','24','Answer 24: Week 1 Section 3 Question 7',1,'Added.',8,1),(57,'2016-05-05 07:57:25.159215','25','Answer 25: Week 1 Section 3 Question 7',1,'Added.',8,1),(58,'2016-05-05 07:57:34.379095','26','Answer 26: Week 1 Section 3 Question 7',1,'Added.',8,1),(59,'2016-05-05 07:58:16.822070','8','Question 8: Week 1 Section 3',1,'Added.',7,1),(60,'2016-05-05 07:59:40.973452','8','Question 8: Week 1 Section 3',2,'Changed question_text.',7,1),(61,'2016-05-05 08:02:31.153069','27','Answer 27: Week 1 Section 3 Question 8',1,'Added.',8,1),(62,'2016-05-05 08:02:46.723092','28','Answer 28: Week 1 Section 3 Question 8',1,'Added.',8,1),(63,'2016-05-05 08:02:55.090600','29','Answer 29: Week 1 Section 3 Question 8',1,'Added.',8,1),(64,'2016-05-05 08:03:04.572924','30','Answer 30: Week 1 Section 3 Question 8',1,'Added.',8,1),(65,'2016-05-05 08:03:59.155052','9','Question 9: Week 1 Section 3',1,'Added.',7,1),(66,'2016-05-05 08:07:58.095932','31','Answer 31: Week 1 Section 3 Question 9',1,'Added.',8,1),(67,'2016-05-05 08:08:11.460703','32','Answer 32: Week 1 Section 3 Question 9',1,'Added.',8,1),(68,'2016-05-05 08:09:16.425203','33','Answer 33: Week 1 Section 3 Question 9',1,'Added.',8,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (9,'admin','logentry'),(11,'auth','group'),(10,'auth','permission'),(12,'auth','user'),(13,'contenttypes','contenttype'),(8,'mathlab','answer'),(5,'mathlab','assignment'),(4,'mathlab','attendance'),(1,'mathlab','course'),(6,'mathlab','gradebook'),(7,'mathlab','question'),(2,'mathlab','section'),(3,'mathlab','student'),(14,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2016-04-29 06:48:26.230871'),(2,'auth','0001_initial','2016-04-29 06:48:32.870031'),(3,'admin','0001_initial','2016-04-29 06:48:34.604631'),(4,'admin','0002_logentry_remove_auto_add','2016-04-29 06:48:35.010910'),(5,'contenttypes','0002_remove_content_type_name','2016-04-29 06:48:35.769372'),(6,'auth','0002_alter_permission_name_max_length','2016-04-29 06:48:36.363187'),(7,'auth','0003_alter_user_email_max_length','2016-04-29 06:48:37.512254'),(8,'auth','0004_alter_user_username_opts','2016-04-29 06:48:37.559135'),(9,'auth','0005_alter_user_last_login_null','2016-04-29 06:48:37.950068'),(10,'auth','0006_require_contenttypes_0002','2016-04-29 06:48:37.965647'),(11,'auth','0007_alter_validators_add_error_messages','2016-04-29 06:48:37.996886'),(12,'mathlab','0001_initial','2016-04-29 06:48:47.982088'),(13,'sessions','0001_initial','2016-04-29 06:48:48.385089'),(14,'mathlab','0002_attendance_att_week_num','2016-05-02 07:13:26.570284'),(15,'mathlab','0003_auto_20160502_1544','2016-05-02 22:44:57.284900'),(16,'mathlab','0004_assignment_course','2016-05-05 07:09:11.791586'),(17,'mathlab','0005_auto_20160505_0036','2016-05-05 07:36:31.075566');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('25uuurvxujnh7pilnf6mv6zik1h75deu','YTA5Y2ZlYWVlY2IwNGU5ZTAwZjY2ZWFmMTdkODI2NTQyM2IzM2MzNDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIwZGU2NzFjOGU3Y2RiMjY1MTcwZWU3ZWI4YjQ4M2E4ZTQ1NGQ5ODEyIn0=','2016-05-16 05:22:39.429738'),('c34u33jta9zkeddb4fj2nmh004vpp2vc','ZTY5MjVmNDhjMjc0YTIyNjBmNzY2NjNkNjUzOWVhODFmMDhjNDZkZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiIwZGU2NzFjOGU3Y2RiMjY1MTcwZWU3ZWI4YjQ4M2E4ZTQ1NGQ5ODEyIn0=','2016-05-16 07:56:12.786903'),('dptyfl76zj0ob40j02v0qq9yop3qt6z1','YWVmYmM4MDcyZTFhMWUwMTkyMDVlMDcxYjUyOTg1Zjk2MzhmNzkwZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMWUxNWQ4MDhkODY2NDdmOGQ5MDAwZDg1NGI2MDBhNDRmNmY1ZmU3ZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=','2016-05-16 23:01:46.469032'),('mmpwkv81wsgt77won4twnu4qq1yfimo4','NDUxN2U3Yzg4MjdmOTM0OGJhMjJmZTI0YzRkNjEwMTBiOTljYzYwMDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2MDBlNzM1MzkyY2U5ZTIzNTJkNDAyYWY3ZWY2MzgzNjQ2ZjA2NGVkIn0=','2016-05-16 23:20:38.583498'),('o2s9fgsl76w8v6rm2okrag8org42kjoh','MWM3NjYzODMxNjVmNTUwYmQ5MzdkZmIxZjE2ZmNlM2E1NmQ5ZGM4Njp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxZTE1ZDgwOGQ4NjY0N2Y4ZDkwMDBkODU0YjYwMGE0NGY2ZjVmZTdkIn0=','2016-05-16 23:25:16.198373'),('sj71y2yjx5igfu241n7pskzbal7zxhsz','Mzk3MGFjM2I3ZDZhYjcwZjIxNzI2MzMwY2U2YmE2NDFmZTUzMDFiNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNjAwZTczNTM5MmNlOWUyMzUyZDQwMmFmN2VmNjM4MzY0NmYwNjRlZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=','2016-05-19 05:52:17.411676'),('ypfq1c9yy4zg6kmr4dx1yhefytq74k5l','OWNkNjljNzVlY2Y3MmFhMWZmMjMzMTNjZTNiODA3MTQxODBiNGUwNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjBkZTY3MWM4ZTdjZGIyNjUxNzBlZTdlYjhiNDgzYThlNDU0ZDk4MTIiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2016-05-19 07:17:14.093618'),('zvw0wzi7812849aqrddxagymmxplgd7o','ZTY5MjVmNDhjMjc0YTIyNjBmNzY2NjNkNjUzOWVhODFmMDhjNDZkZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiIwZGU2NzFjOGU3Y2RiMjY1MTcwZWU3ZWI4YjQ4M2E4ZTQ1NGQ5ODEyIn0=','2016-05-16 07:55:46.871125');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mathlab_answer`
--

DROP TABLE IF EXISTS `mathlab_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mathlab_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer_text` varchar(500) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_correctness` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mathlab_answer_7aa0f6ee` (`question_id`),
  CONSTRAINT `mathlab_answer_question_id_09e8e705_fk_mathlab_question_id` FOREIGN KEY (`question_id`) REFERENCES `mathlab_question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mathlab_answer`
--

LOCK TABLES `mathlab_answer` WRITE;
/*!40000 ALTER TABLE `mathlab_answer` DISABLE KEYS */;
INSERT INTO `mathlab_answer` VALUES (1,'692',1,0),(2,'448',1,1),(3,'121',1,0),(4,'112',1,0),(5,'12',2,0),(6,'34',2,0),(7,'46',2,0),(8,'42',2,1),(9,'9',3,0),(10,'19',3,1),(11,'24',3,0),(12,'39',3,0),(13,'100',4,0),(14,'300',4,1),(15,'600',4,0),(16,'900',4,0),(17,'5/2',5,0),(18,'-5/2',5,1),(19,'5',5,0),(20,'-5',5,0),(21,'21 gallons',6,0),(22,'52 gallons',6,0),(23,'63 gallons',6,1),(24,'100',7,0),(25,'821',7,0),(26,'921',7,1),(27,'77',8,0),(28,'90',8,0),(29,'82',8,1),(30,'85.77',8,0),(31,'4 1/2 hrs',9,0),(32,'11 1/4 hrs',9,1),(33,'7 1/2 hrs',9,0);
/*!40000 ALTER TABLE `mathlab_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mathlab_assignment`
--

DROP TABLE IF EXISTS `mathlab_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mathlab_assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_week` int(11) NOT NULL,
  `assignment_section` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mathlab_assignment_ea134da7` (`course_id`),
  CONSTRAINT `mathlab_assignment_course_id_3fd94c05_fk_mathlab_course_id` FOREIGN KEY (`course_id`) REFERENCES `mathlab_course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mathlab_assignment`
--

LOCK TABLES `mathlab_assignment` WRITE;
/*!40000 ALTER TABLE `mathlab_assignment` DISABLE KEYS */;
INSERT INTO `mathlab_assignment` VALUES (1,1,1,1),(2,1,2,1),(3,1,3,1);
/*!40000 ALTER TABLE `mathlab_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mathlab_attendance`
--

DROP TABLE IF EXISTS `mathlab_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mathlab_attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `att_week_num` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mathlab_attendance_30a811f6` (`student_id`),
  CONSTRAINT `mathlab_attendance_student_id_4e4ec2c5_fk_mathlab_student_id` FOREIGN KEY (`student_id`) REFERENCES `mathlab_student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mathlab_attendance`
--

LOCK TABLES `mathlab_attendance` WRITE;
/*!40000 ALTER TABLE `mathlab_attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `mathlab_attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mathlab_course`
--

DROP TABLE IF EXISTS `mathlab_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mathlab_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_code` varchar(10) NOT NULL,
  `course_name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mathlab_course`
--

LOCK TABLES `mathlab_course` WRITE;
/*!40000 ALTER TABLE `mathlab_course` DISABLE KEYS */;
INSERT INTO `mathlab_course` VALUES (1,'MATH 94','Introduction to Algebra'),(2,'MATH 95','Intermediate Algebra');
/*!40000 ALTER TABLE `mathlab_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mathlab_gradebook`
--

DROP TABLE IF EXISTS `mathlab_gradebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mathlab_gradebook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_total` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mathlab_gradeboo_assignment_id_b2df32f5_fk_mathlab_assignment_id` (`assignment_id`),
  KEY `mathlab_gradebook_30a811f6` (`student_id`),
  CONSTRAINT `mathlab_gradebook_student_id_967151a0_fk_mathlab_student_id` FOREIGN KEY (`student_id`) REFERENCES `mathlab_student` (`id`),
  CONSTRAINT `mathlab_gradeboo_assignment_id_b2df32f5_fk_mathlab_assignment_id` FOREIGN KEY (`assignment_id`) REFERENCES `mathlab_assignment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mathlab_gradebook`
--

LOCK TABLES `mathlab_gradebook` WRITE;
/*!40000 ALTER TABLE `mathlab_gradebook` DISABLE KEYS */;
/*!40000 ALTER TABLE `mathlab_gradebook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mathlab_question`
--

DROP TABLE IF EXISTS `mathlab_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mathlab_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_text` varchar(1000) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mathlab_question_assignment_id_f52cde35_fk_mathlab_assignment_id` (`assignment_id`),
  CONSTRAINT `mathlab_question_assignment_id_f52cde35_fk_mathlab_assignment_id` FOREIGN KEY (`assignment_id`) REFERENCES `mathlab_assignment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mathlab_question`
--

LOCK TABLES `mathlab_question` WRITE;
/*!40000 ALTER TABLE `mathlab_question` DISABLE KEYS */;
INSERT INTO `mathlab_question` VALUES (1,'A gorilla eats 4 bananas every hour it is awake. If he sleeps 8 hours every night, how many bananas will he eat in a week?',1),(2,'How many inches are in 3 and a half feet?',1),(3,'A farmer sells his apples by the barrel. If he sells 1482 apples and there are 78 apples in each barrel, how many barrels did he sell?',1),(4,'George is reading a 900 page novel. He has twice as many pages left to read as he has already read. How many pages has he read?',2),(5,'Five times some number is 10 less than that same number. What is the number?',2),(6,'It takes 42 gallons of water to produce 1 pound of broccoli. If this is twice the amount of water it takes for lettuce, how many gallons of water does it take to produce 3 pounds of lettuce?',2),(7,'Billy\'s mom is not happy. The bill for Billy\'s cell phone was $162.52 this month. If the monthly service charge is $22.95 and Billy gets 100 minutes free, how many minutes did Billy use his cell phone if he gets charged 17 cents a minute after the first 100?',3),(8,'Justin wants a grade of A in his algebra class. On his first 4 tests he raises his score by 5 points each time. For an A, Justin must average 90 on all 5 tests. Justin figures out he must score a 92 on his final test to get an A. What did he score on his first test?',3),(9,'Due to wind conditions it takes one and a half times as long for Greg to fly from Beaver City to Wrigley Falls than from Wrigley Falls to Beaver City. If Greg is one third of the way to Beaver City after an hour and a half, how many hours is the round trip?',3);
/*!40000 ALTER TABLE `mathlab_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mathlab_section`
--

DROP TABLE IF EXISTS `mathlab_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mathlab_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_info` varchar(20) NOT NULL,
  `course_id` int(11) NOT NULL,
  `professor_id` int(11) NOT NULL,
  `section_num` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mathlab_section_course_id_fd28b0f3_fk_mathlab_course_id` (`course_id`),
  KEY `mathlab_section_professor_id_78259e10_fk_auth_user_id` (`professor_id`),
  CONSTRAINT `mathlab_section_course_id_fd28b0f3_fk_mathlab_course_id` FOREIGN KEY (`course_id`) REFERENCES `mathlab_course` (`id`),
  CONSTRAINT `mathlab_section_professor_id_78259e10_fk_auth_user_id` FOREIGN KEY (`professor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mathlab_section`
--

LOCK TABLES `mathlab_section` WRITE;
/*!40000 ALTER TABLE `mathlab_section` DISABLE KEYS */;
INSERT INTO `mathlab_section` VALUES (1,'Mo, Tu 9:00-10:15',1,3,1);
/*!40000 ALTER TABLE `mathlab_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mathlab_student`
--

DROP TABLE IF EXISTS `mathlab_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mathlab_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mathlab_student_section_id_09741c2b_fk_mathlab_section_id` (`section_id`),
  KEY `mathlab_student_student_id_c5f967f1_fk_auth_user_id` (`student_id`),
  CONSTRAINT `mathlab_student_section_id_09741c2b_fk_mathlab_section_id` FOREIGN KEY (`section_id`) REFERENCES `mathlab_section` (`id`),
  CONSTRAINT `mathlab_student_student_id_c5f967f1_fk_auth_user_id` FOREIGN KEY (`student_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mathlab_student`
--

LOCK TABLES `mathlab_student` WRITE;
/*!40000 ALTER TABLE `mathlab_student` DISABLE KEYS */;
INSERT INTO `mathlab_student` VALUES (1,1,2);
/*!40000 ALTER TABLE `mathlab_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'mathlabdb'
--

--
-- Dumping routines for database 'mathlabdb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-15 18:03:29
