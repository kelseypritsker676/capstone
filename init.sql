BEGIN;
--
-- Create model Answer
--
CREATE TABLE `mathlab_answer` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `answer_text` varchar(200) NOT NULL);
--
-- Create model Assignment
--
CREATE TABLE `mathlab_assignment` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `assignment_week` integer NOT NULL, `assignment_section` integer NOT NULL);
--
-- Create model Attendance
--
CREATE TABLE `mathlab_attendance` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY);
--
-- Create model Course
--
CREATE TABLE `mathlab_course` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `course_code` varchar(10) NOT NULL, `course_name` varchar(60) NOT NULL);
--
-- Create model GradeBook
--
CREATE TABLE `mathlab_gradebook` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `grade_total` integer NOT NULL, `assignment_id` integer NOT NULL);
--
-- Create model Question
--
CREATE TABLE `mathlab_question` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `question_text` varchar(200) NOT NULL, `assignment_id` integer NOT NULL);
--
-- Create model Section
--
CREATE TABLE `mathlab_section` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `meeting_info` varchar(20) NOT NULL, `course_id` integer NOT NULL, `professor_id` integer NOT NULL);
--
-- Create model Student
--
CREATE TABLE `mathlab_student` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `section_id` integer NOT NULL, `student_id` integer NOT NULL);
--
-- Add field student to gradebook
--
ALTER TABLE `mathlab_gradebook` ADD COLUMN `student_id` integer NOT NULL;
ALTER TABLE `mathlab_gradebook` ALTER COLUMN `student_id` DROP DEFAULT;
--
-- Add field student to attendance
--
ALTER TABLE `mathlab_attendance` ADD COLUMN `student_id` integer NOT NULL;
ALTER TABLE `mathlab_attendance` ALTER COLUMN `student_id` DROP DEFAULT;
--
-- Add field question to answer
--
ALTER TABLE `mathlab_answer` ADD COLUMN `question_id` integer NOT NULL;
ALTER TABLE `mathlab_answer` ALTER COLUMN `question_id` DROP DEFAULT;
ALTER TABLE `mathlab_gradebook` ADD CONSTRAINT `mathlab_gradeboo_assignment_id_b2df32f5_fk_mathlab_assignment_id` FOREIGN KEY (`assignment_id`) REFERENCES `mathlab_assignment` (`id`);
ALTER TABLE `mathlab_question` ADD CONSTRAINT `mathlab_question_assignment_id_f52cde35_fk_mathlab_assignment_id` FOREIGN KEY (`assignment_id`) REFERENCES `mathlab_assignment` (`id`);
ALTER TABLE `mathlab_section` ADD CONSTRAINT `mathlab_section_course_id_fd28b0f3_fk_mathlab_course_id` FOREIGN KEY (`course_id`) REFERENCES `mathlab_course` (`id`);
ALTER TABLE `mathlab_section` ADD CONSTRAINT `mathlab_section_professor_id_78259e10_fk_auth_user_id` FOREIGN KEY (`professor_id`) REFERENCES `auth_user` (`id`);
ALTER TABLE `mathlab_student` ADD CONSTRAINT `mathlab_student_section_id_09741c2b_fk_mathlab_section_id` FOREIGN KEY (`section_id`) REFERENCES `mathlab_section` (`id`);
ALTER TABLE `mathlab_student` ADD CONSTRAINT `mathlab_student_student_id_c5f967f1_fk_auth_user_id` FOREIGN KEY (`student_id`) REFERENCES `auth_user` (`id`);
CREATE INDEX `mathlab_gradebook_30a811f6` ON `mathlab_gradebook` (`student_id`);
ALTER TABLE `mathlab_gradebook` ADD CONSTRAINT `mathlab_gradebook_student_id_967151a0_fk_mathlab_student_id` FOREIGN KEY (`student_id`) REFERENCES `mathlab_student` (`id`);
CREATE INDEX `mathlab_attendance_30a811f6` ON `mathlab_attendance` (`student_id`);
ALTER TABLE `mathlab_attendance` ADD CONSTRAINT `mathlab_attendance_student_id_4e4ec2c5_fk_mathlab_student_id` FOREIGN KEY (`student_id`) REFERENCES `mathlab_student` (`id`);
CREATE INDEX `mathlab_answer_7aa0f6ee` ON `mathlab_answer` (`question_id`);
ALTER TABLE `mathlab_answer` ADD CONSTRAINT `mathlab_answer_question_id_09e8e705_fk_mathlab_question_id` FOREIGN KEY (`question_id`) REFERENCES `mathlab_question` (`id`);

COMMIT;
